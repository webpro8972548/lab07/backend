export declare class User {
    id: number;
    email: string;
    password: string;
    fullName: string;
    gender: 'male' | 'female' | 'others';
}
