export declare class CreateUserDto {
    email: string;
    password: string;
    fullName: string;
    gender: 'male' | 'female' | 'others';
}
