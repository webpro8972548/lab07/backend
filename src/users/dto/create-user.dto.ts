import {
  
  IsEmail,
  IsNotEmpty,
  Length,
} from 'class-validator';
export class CreateUserDto {
  @IsEmail()
  email: string;
  @IsNotEmpty()
  @Length(8, 32)
  password: string;
  fullName: string;

 // @IsArray()
 // @ArrayNotEmpty()
  //roles: ('admin' | 'user')[];
  @IsNotEmpty()
  gender: 'male' | 'female' | 'others';
}
